function addNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum + secondNum;

    document.getElementById('result').innerHTML="The sum is " + result;
    return false;
}

function subNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum - secondNum;

    document.getElementById('result').innerHTML="The difference is " + result;
    return false;
}

function multiplyNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum * secondNum;

    document.getElementById('result').innerHTML="The product is " + result;
    return false;
}

function divideNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum / secondNum;

    document.getElementById('result').innerHTML="The quotient is " + result;
    return false;
}
